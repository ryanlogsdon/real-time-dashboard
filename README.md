# Building a real-time monitoring system in Angular 11, Firebase, and Chart.js

# Overview
1. Create a Firebase app
1. Create a nearly empty Angular app
1. Connect Firebase to the Angular app
1. Add Chart.js
1. Simulate sensor data to write to Firebase

# Create your Firebase App
The Firebase website changes fairly often, so the exact wording below might change a little.

We're going to use Cloud Firestore to build a NoSQL database to store time-buckets of sensor data.

* firebase.google.com > (login) > "Go to console" > "Add project" > name: "plants" >
    Continue > (we're not going to enable Google Analytics) > "Create project" 
    
* when the project is created, you'll be taken to its dashboard, click the small "</>" Web logo > 
    enter "plants" for "My web app" name > (no need to set up Firebase hosting) > "Register app"

* you'll be taken to a page with Firebase SDK code. keep this tab open! we'll work with the `firebaseConfig` variable in a later step when we connect our Firebase and Angular apps

## Set up the Cloud Firestore database
* in the left column, browse to Build > Cloud Firestore > click "Create database" > "Start in test mode" > "Next" > (choose a location close to you) > "Enable"

## Cloud Firestore test data
* you don't have to set up any data, collections, or documents manually! Everything will be automatically built from our Angular app

* for this project, I have 1 collection of documents called `gardenPlants`

* there's a special document with the id `current` which holds all sensors' newest readings, and it's constantly being
overwritten

* all other docs take the id `[YYYY]_[MM]_[DD]_h[HH]` (ex: `2021_05_14_h12`) to signify that all sensor readings of the
given time period are stored here



# Create your Angular App

## Make sure your coding environment is up to date
* make sure you're at least using Angular 12.0.2
> ng version            

* if you need to upgrade Angular
> npm install -g @angular/cli

* make sure you're at least using npm version 7.14.0
> npm -v                


## Create the Angular project

> ng new real-time-dashboard --strict false

> cd real-time-dashboard

> npm install chart.js

* helper: https://www.chartjs.org/docs/latest/developers/updates.html

## Add the Firebase and Angular Material libraries

> ng add @angular/fire
* Please select a project: (the newly created Firebase "plants" app)

> ng add @angular/material
* Choose a prebuilt theme: (any)
* Typography: Yes
* Browser animations: (yes or no; I'll be choosing yes)

Note: you can choose "No", but even if you choose "Yes" and end up
    not using it, when you build your app for production, all unused libraries will be removed


## Start the Server
* open a new command prompt tab to run the next command
> ng serve --port 4202 --open

## Remove boilerplate code
* in `realtimedash/src/app/app.component.html`, only keep the `<router-outlet></router-outlet>` tag at the very bottom of the file

## Add components, services, Firebase auth, and Angular page guards
> ng generate component home

## Link the pages (Set up page routing)
* open `realtimedash/src/app/app-routing.module.ts`
    * import each component
    * add each component to the `routes:[...]` array
    
## Use the Firebase App credentials
* open `realtimedash/src/environments/environment.ts` and `environment.prod.ts`
* in your browser, return to the tab with the Firebase SDK code and the `firebaseConfig` variable; copy the snippet of code to both `environment` files, renaming `firebaseConfig` to `firebase`
* in case you're using `git`, you won't want to publish this information, so open your `.gitignore` file, make sure to include the line `/src/environments/**`
* note: since these files are not part of my GitLab project, here's what my `environment.ts` file generally looks like:

```
export const environment = {
    production: false,
    firebase: {
        apiKey: "MY_DATA_HERE",
        authDomain: "MY_DATA_HERE",
        projectId: "MY_DATA_HERE",
        storageBucket: "MY_DATA_HERE",
        messagingSenderId: "MY_DATA_HERE",
        appId: "MY_DATA_HERE"
    }
};
```

* keep in mind that Firebase often changes what info is put inside the `firebaseConfig = {...}` variable, so if Firebase supplies you with slightly different info, that's fine

## Add basic navigation to each component
* open `angular-firebase-dashboard/src/app/app.module.ts`
    * import the additional modules that we'll be using (see my code)
    * add these modules to the `imports:[...]` array
    * this lets us use Angular Material in our components and it imports our Firebase environment variables


## Add graphs to the component
* open `realtimedash/src/app/home/home.component.ts` and `.html`
    * import the code

